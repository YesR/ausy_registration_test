<?php

namespace Drupal\ausy_dev;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service helper.
 */
class Helper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Helper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get count registration .
   */
  public function getCountRegistration() {
    return $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'registration')
      ->count()
      ->execute();
  }

}
