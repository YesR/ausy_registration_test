<?php

namespace Drupal\ausy_dev\Form;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * Route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a \Drupal\Core\Menu\MenuParentFormSelector.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Component\Utility\EmailValidator $email_validator
   *   Email validator service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EmailValidator             $email_validator,
    RouteMatchInterface        $route_match
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->emailValidator = $email_validator;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
      $container->get('email.validator'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ausy_dev_registration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $department = "";
    if ($department_parm = $this->routeMatch->getParameter('department')) {
      $department_entity = $this->entityTypeManager
        ->getStorage('department')
        ->loadByProperties([
          'departement' => $department_parm,
        ]);
      if (empty($department_entity)) {
        throw new NotFoundHttpException();
      }
      else {
        $department_entity = reset($department_entity);
        $department = $department_entity->get('departement')
          ->getValue()[0]['value'];
      }
    }

    $form['employee_name'] = [
      '#type' => 'textfield',
      '#title' => 'Name of the employee',
      '#required' => TRUE,
    ];

    $form['department'] = [
      '#type' => 'textfield',
      '#title' => 'Department',
      '#default_value' => $department,
      '#required' => TRUE,
    ];

    $form['amount_kids'] = [
      '#type' => 'number',
      '#title' => 'Amount of kids',
      '#required' => TRUE,
    ];

    $form['amount_vegetarians'] = [
      '#type' => 'number',
      '#title' => 'Amount of vegetarians',
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Email address',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_data = $form_state->getValues();

    $value = trim($form_data['email']);

    if (!$this->emailValidator->isValid($value)) {
      $form_state->setErrorByName('email', "The email address $value is not valid.");
    }

    if ((int) $form_data['amount_vegetarians'] > ((int) $form_data['amount_kids'] + 1)) {
      $form_state->setErrorByName('amount_vegetarians', "The Amount of vegetarians should not higher than people.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $storage = $this->entityTypeManager->getStorage('node');
    $form_data = $form_state->getValues();

    $query = $storage->loadByProperties([
      'type' => 'registration',
      'field_email_address' => $form_data['email'],
    ]);

    if (empty($query)) {
      $registration = $storage->create([
        'type' => 'registration',
        'title' => $form_data['employee_name'],
        'field_amount_kids' => $form_data['amount_kids'],
        'field_amount_vegetarians' => $form_data['amount_vegetarians'],
        'field_email_address' => $form_data['email'],
      ]);
      $registration->save();
      $this->messenger()->addStatus('The registration is well added');
    }
    else {
      $this->messenger()
        ->addStatus('The registration with email: ' . $form_data['email'] . ' is already exit');
    }

  }

}
