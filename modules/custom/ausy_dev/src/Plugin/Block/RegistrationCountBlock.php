<?php

namespace Drupal\ausy_dev\Plugin\Block;

use Drupal\ausy_dev\Helper;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Registration count block .
 *
 * @Block(
 *   id = "registration_count_block",
 *   admin_label = @Translation("Registration count block"),
 *   category = @Translation("Custom")
 * )
 */
class RegistrationCountBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The ausy_dev.helper service.
   *
   * @var \Drupal\ausy_dev\Helper
   */
  protected $helper;

  /**
   * Constructs.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ausy_dev\Helper $helper
   *   The ausy_dev.helper service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Helper $helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ausy_dev.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'registration_count',
      '#count' => $this->helper->getCountRegistration(),
    ];
  }

}
