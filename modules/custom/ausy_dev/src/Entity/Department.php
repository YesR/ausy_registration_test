<?php

namespace Drupal\ausy_dev\Entity;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Department entity class.
 *
 * @ContentEntityType(
 *   id = "department",
 *   label = @Translation("Department"),
 *   base_table = "department",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *
 * )
 */
class Department extends ContentEntityBase implements ContentEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('ID'))
      ->setReadOnly(TRUE);

    $fields['departement'] = BaseFieldDefinition::create('string')
      ->setDescription(t('Department'))
      ->setRequired(TRUE)
      ->setLabel(t('Department'));

    return $fields;
  }

}
